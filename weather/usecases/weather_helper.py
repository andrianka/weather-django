from weather.models import City
from decouple import config
import requests


class CityHelper:

    def __init__(self, search_request_city=None):
        self.request = search_request_city

    def get_cities(self, quantity=3):
        return City.objects.all()[:quantity]

    def weather_api_request(self, city):
        weather_appid = config('WEATHER_APP_ID')
        url = "http://api.openweathermap.org/data/2.5/weather?q={}\
            &units=metric&appid=" + weather_appid
        res = requests.get(url.format(city)).json()
        return res

    def format_weather_result(self, city):
        res = self.weather_api_request(city)
        info = {
            'city': city,
            'temp': res['main']['temp'],
            'icon': res['weather'][0]['icon']
        }
        return info

    def get_city_info_from_search(self):
        return [self.format_weather_result(self.request)]

    def get_cities_info_from_db(self):
        cities_info=[]
        for city in self.get_cities():
            info = self.format_weather_result(city)
            cities_info.append(info)
        return cities_info

    def get_all_info(self):
        return self.get_city_info_from_search() + self.get_cities_info_from_db()
