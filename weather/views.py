from django.shortcuts import render
from .usecases.weather_helper import CityHelper


def home(request):
    if request.method == "POST":
        city_params= request.POST['city']
        cities_info = CityHelper(city_params).get_all_info()
        context = {'cities_info': cities_info}
    else:
        cities_info = CityHelper().get_cities_info_from_db()
        context = {'cities_info': cities_info}
    return render(request, 'weather/home.html', context)
